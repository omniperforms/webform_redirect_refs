<?php 

/**
 * Builds the Webform Redirect Referrals settings form.
 * @param $form
 * @param $form_state
 */

function webform_redirect_refs_admin_form($form, &$form_state) {

	$form['webform_redirect_refs_list'] = array(
		'#title' => t('Referer list'),
		'#dsecription' => t('Enter each referer on a new line.'),
		'#type' => 'textarea',
		'#default_value' => variable_get('block_referer_list', ''),
	);

	return system_settings_form($form);
}